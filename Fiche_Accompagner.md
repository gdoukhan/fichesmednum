# Accompagner l’usager

## Voir aussi :
- Posture : Identifier les publics
- Posture : Les clés de l’accueil
- Kit territoire / Cas de ré-orientation des publics

*Certains publics viendront vers vous avec une demande d’accompagnement intégral : “Je ne sais / peux pas faire, faites à ma place”. Vous devez être préparé à cette situation et y réagir au mieux, en sécurisant votre accompagnement et en proposant des réponses à l’usager.*

## Pour répondre aux usagers
> La première réponse est l’écoute et la bienveillance envers l’usager, soyez cordial, reformulez ses demandes et référez vous à la fiche clés de l’accueil.
> De manière générale, faites comprendre à l’usager que son intérêt, si il en a les capacités, est d’accepter le fait que les démarches soient en ligne, et de s’autonomiser au maximum, référez vous à la fiche Identifier les publics.

### Afin d’accompagner l’usager, faites avec lui et non à sa place :
- Ne faites pas de l’écran une barrière entre vous et l’usager, mais mettez le de manière à ce qu’il soit visible pour vous et pour lui.
- Sauf cas particulier c’est l’usager et non le médiateur, qui a la maîtrise du clavier et de la souris.
- Assurez-vous qu’il dispose de l’ensemble des informations nécessaires avant de démarrer la procédure.
- Faites toujours valider au maximum par l’usager, a minima appuyer sur le bouton de validation en fin de procédure, après lui avoir redit oralement l’intégralité des éléments contenus et leur implication. (coût, mode d’envoi des documents, délai...)
- Imprimez à l’usager l’ensemble des éléments récapitulatifs de sa démarche.
- Si besoin fournissez lui un exemplaire papier de la fiche procédure adéquate, pour qu’il comprenne quelles seront les prochaines étapes.
- Proposez-lui un pense-bête administratif pour son mot de passe, ou assurez-vous qu’il soit en capacité de se reconnecter avec France Connect.

### Public qui a peur du numérique
Demandez à la personne de faire le minimum avec vous, expliquez-lui simplement la démarche, rappelez-lui que l’ensemble du processus est sécurisé. Dites-lui dès le départ qu’elle aura un récapitulatif papier.
Expliquez-lui que de plus en plus de démarches ne pourront se faire qu’en ligne (horizon 2022, 100% des démarches administratives), et demandez-lui si un accompagnement au numérique plus général pourrait l’intéresser. (cf. tableau des ré orientations des publics)*

### Public empêché
Prenez bien en compte la spécificité de la personne, mais n’appuyez pas sur ses difficultés, agissez discrètement et avec bienveillance.
- Dans le cas d’une personne à mobilité réduite dirigez-la vers l’espace adapté et autonomisez-la.
- Dans le cas d’une personne en situation de handicap sensoriel ou mental, demandez à la personne s’il lui est possible de revenir avec un aidant. Dans le cas contraire, voyez si vous êtes en capacité de communiquer clairement avec elle et de disposer de tous les éléments,sinon réorientez-la vers une structure adaptée. (cf. tableau des ré-orientations des publics)

### Public ne maîtrisant pas la langue française à l’écrit
Pour ce type de public, prenez le temps de leur expliquer la procédure, de leur lire intégralement les différentes informations présentes à l’écran, et de les laisser écrire au maximum si elles sont en capacité de le faire. A minima laissez-les valider les différentes étapes, en leur expliquant clairement ce qui a été renseigné.
Eventuellement, si la personne en a l’habitude, accompagnez-la dans sa démarche depuis son smartphone en utilisant les outils de reconnaissance vocale dont elle se sert.
Laissez-lui systématiquement un justificatif papier, qu’elle pourra relire avec ses proches. Et si cela se présente, demandez-lui avec tact si un accompagnement au numérique et à la langue française pourrait l’intéresser. (cf. tableau des ré-orientations des publics)

## Pour se sécuriser en tant que médiateur

### Responsabilité
La responsabilité juridique de l’Etat peut être engagée en cas de faute averée lors de l’accompagnement (communication d’un montant ou d’un délai erroné...), on appelle cela une faute de service mais votre responsabilité n’est pas engagée sauf à sortir complètement du cadre de vos missions et entrer dans le cadre juridique d’une faute personnelle.
Pour vous protéger, autonomisez au maximum vos usagers, a minima faites leur valider la démarche, imprimez leur tout récapitulatif, ne conservez aucune donnée personnelle après leur passage, ayez le moins possible accès à leurs mots de passe, modes de paiement ou autres informations personnelles, ne vous engagez pas sur le résultat ou le délai incertain d’une téléprocédure en cours, ne divulguez aucune information personnelle.

### France Connect Aidants
Le dispositif France Connect Aidants devrait permettre courant 2019 d’accompagner les usagers les plus en difficulté en pouvant réellement faire les démarches à leur place, en toute sécurité informatique et juridique.

## Ressources utiles :
Pour aller plus loin / à conseiller aux usagers
- La formation Posture et les Personae de la DITP
- [Le kit d’intervention rapide issu de la Stratégie Nationale pour un Numérique Inclusif, notamment le volet 3 “J’accompagne”](https://kit-inclusion.societenumerique.gouv.fr/)
[Le cahier de l’ANSA sur les personnes ne maitrisant pas l’écrit en langue française :](https://frama.link/ANSA-Illettrisme)