# fichesmednum

Fiches mémento / autoformation sur l'accompagnement numérique issues de productions réalisées dans le cadre d'un appel d'offres pour le Ministère de l'Intérieur, coporté par la SCIC LaMedNum, l'ADRETS, et ICI

Juin 2019, https://lamednum.coop https://adrets-asso.fr https://associationici.fr

L'ensemble des fiches sont portées sous licence Creative Commons Attribution Partage dans les mêmes conditions, CC-By-SA 3.0 https://creativecommons.org/licenses/by-sa/3.0/fr/

Dépôt des fiches: https://framagit.org/gdoukhan/fichesmednum/